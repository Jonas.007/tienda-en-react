import React, { useState, useEffect, createContext } from "react";
import Data from "../Data.js";

export const DataContext = createContext();

export const DataProvider = (props) => {
  const [productos, setProductos] = useState([]);
  const [menu, setMenu] = useState(false);
  const [carrito, setCarrito] = useState([]);
  const [total, setTotal] = useState(0);
  const [clientData, setClientData] = useState({
    nombres: "",
    apellidos: "",
    email: "",
    confirmarEmail: "",
    region: "",
    ciudad: "",
    direccion: "", 
    codigoPostal: ""
  });

  useEffect(() => {
    const dataCarrito = JSON.parse(localStorage.getItem("dataCarrito"));
    if (dataCarrito) setCarrito(dataCarrito);

    const producto = Data.items;
    if (producto) setProductos(producto);
  }, []);

  useEffect(() => {
    localStorage.setItem("dataCarrito", JSON.stringify(carrito));
    const getTotal = () => {
      const total = carrito.reduce((prev, item) => {
        return prev + item.price * item.cantidad;
      }, 0);
      setTotal(total);
    };
    getTotal();
  }, [carrito]);

  const addCarrito = (id) => {
    const check = carrito.every((item) => item.id !== id);
    if (check) {
      const data = productos.filter((producto) => producto.id === id);
      setCarrito([...carrito, ...data]);
    } else {
      alert("El producto se ha  añadido al carrito");
    }
  };

  const removeProducto = (id) => {
    if (window.confirm("¿Quieres suspender el producto? ")) {
      carrito.forEach((item, index) => {
        if (item.id === id) {
          item.cantidad = 1;
          carrito.splice(index, 1);
        }
      });
      setCarrito([...carrito]);
    }
  };

  const value = {
    menu: [menu, setMenu],
    carrito: [carrito, setCarrito],
    client: [clientData, setClientData],
    total: [total, setTotal],
    productos: [productos],
    productos: [productos],
    addCarrito: addCarrito,
    removeProducto: removeProducto,
  };

  return (
    <DataContext.Provider value={value}>{props.children}</DataContext.Provider>
  );
};
