import React from "react";
import { Switch, Route } from "react-router-dom";
import { Inicio } from "./Inicio";
import { Productos } from "./Productos";
import PostPago from "./PostPago";

export const Paginas = () => {
  return (
    <section>
      <Switch>
        <Route path="/" exact component={Inicio} />
        <Route path="/productos" exact component={Productos} />
        <Route path="/post-pago" exact component={PostPago} />
      </Switch>
    </section>
  );
};
