import React, { useContext } from "react";
import { DataContext } from "../../context/Dataprovider";
import "./styles.css";

const PostPago = () => {
  const value = useContext(DataContext);
  const [clientData, setClientData] = value.client;

  const submitHandler = (e) => {
    e.preventDefault();
  };

  const dataHanldler = (e) => {
    setClientData({ ...clientData, [e.target.name]: e.target.value });
  };

  return (
    <>
      <h1 className="title">POST_PAGO</h1>
      <div className="main-card">
        <h1>Complete sus datos para realizar la compra</h1>
        <form onSubmit={submitHandler}>
          <div className="form-divider">
            <div>
              <div className="form-group">
                <span>Nombres</span>
                <input
                  type="text"
                  value={clientData.nombres}
                  onChange={dataHanldler}
                  name="nombres"
                  placeholder="Nombres"
                />
              </div>
              <div className="form-group">
                <span>Correo electronico</span>
                <input
                  type="text"
                  name="email"
                  value={clientData.email}
                  onChange={dataHanldler}
                  placeholder="Email"
                />
              </div>
              <div className="form-group">
                <span>Region</span>
                <input
                  type="text"
                  name="region"
                  value={clientData.region}
                  onChange={dataHanldler}
                  placeholder="Region"
                />
              </div>
              <div className="form-group">
                <span>Direccion</span>
                <input
                  type="text"
                  value={clientData.direccion}
                  onChange={dataHanldler}
                  name="direccion"
                  placeholder="Calle/Depto/Piso/Numero"
                />
              </div>
            </div>
            <div>
              <div className="form-group">
                <span>Apellidos</span>
                <input
                  type="text"
                  name="apellidos"
                  value={clientData.apellidos}
                  onChange={dataHanldler}
                  placeholder="Apellidos"
                />
              </div>
              <div className="form-group">
                <span>Confirme correo electronico</span>
                <input
                  value={clientData.confirmarEmail}
                  onChange={dataHanldler}
                  type="text"
                  name="confirmarEmail"
                  placeholder="Email"
                />
              </div>
              <div className="form-group">
                <span>Ciudad</span>
                <input
                  type="text"
                  name="ciudad"
                  value={clientData.ciudad}
                  onChange={dataHanldler}
                  placeholder="Ciudad"
                />
              </div>
              <div className="form-group">
                <span>Codigo postal</span>
                <input
                  type="text"
                  value={clientData.codigoPostal}
                  onChange={dataHanldler}
                  name="codigoPostal"
                  placeholder="3843875"
                />
              </div>
            </div>
          </div>
          <button type="submit" className="btn-submit">
            Realizar pedido
          </button>
        </form>
      </div>
    </>
  );
};

export default PostPago;
